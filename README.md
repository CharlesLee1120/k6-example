# k6-example
[K6](https://k6.io/) 是一种现代负载测试工具，基于在负载和性能测试行业的多年经验。它提供了干净、平易近人的脚本以及灵活的配置。

## 前置条件

k6安装在你的Windows、Linux或Mac OS, 验证安装请使用下面的命令

```
$ k6 version

k6 v0.36.0 (2022-01-24T09:50:03+0000/ff3f8df, go1.17.6, windows/amd64)
```

## 快速开始

```
$ k6 run .\openfaas-load-test.js


          /\      |‾‾| /‾‾/   /‾‾/
     /\  /  \     |  |/  /   /  /
    /  \/    \    |     (   /   ‾‾\
   /          \   |  |\  \ |  (‾)  |
  / __________ \  |__| \__\ \_____/ .io

  execution: local
     script: .\openfaas-load-test.js
     output: -

  scenarios: (100.00%) 1 scenario, 300 max VUs, 6m0s max duration (incl. graceful stop):
           * default: Up to 300 looping VUs for 5m30s over 6 stages (gracefulRampDown: 30s, gracefulStop: 30s)


running (5m30.0s), 000/300 VUs, 458910 complete and 0 interrupted iterations
default ✓ [======================================] 000/300 VUs  5m30s

     data_received..................: 121 MB 366 kB/s
     data_sent......................: 59 MB  179 kB/s
     http_req_blocked...............: avg=2.08µs   min=0s      med=0s      max=4.33ms  p(90)=0s       p(95)=0s
     http_req_connecting............: avg=548ns    min=0s      med=0s      max=4.33ms  p(90)=0s       p(95)=0s
   ✓ http_req_duration..............: avg=120.67ms min=504.1µs med=60.01ms max=3.74s   p(90)=293.17ms p(95)=439.23ms
       { expected_response:true }...: avg=120.67ms min=504.1µs med=60.01ms max=3.74s   p(90)=293.17ms p(95)=439.23ms
     http_req_failed................: 0.00%  ✓ 0           ✗ 458910
     http_req_receiving.............: avg=59.68µs  min=0s      med=0s      max=24.3ms  p(90)=323.6µs  p(95)=495.1µs
     http_req_sending...............: avg=10.23µs  min=0s      med=0s      max=16.36ms p(90)=0s       p(95)=0s
     http_req_tls_handshaking.......: avg=0s       min=0s      med=0s      max=0s      p(90)=0s       p(95)=0s
     http_req_waiting...............: avg=120.6ms  min=504.1µs med=60.01ms max=3.74s   p(90)=293.13ms p(95)=439.19ms
     http_reqs......................: 458910 1390.630041/s
     iteration_duration.............: avg=120.76ms min=504.1µs med=60.01ms max=3.74s   p(90)=293.38ms p(95)=439.34ms
     iterations.....................: 458910 1390.630041/s
     vus............................: 1      min=1         max=300
     vus_max........................: 300    min=300       max=300
```
